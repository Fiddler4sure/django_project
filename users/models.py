from django.db import models
from django.contrib.auth.models import User
from PIL import Image
from config import Config
import geocoder

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    msisdn = models.CharField(max_length=20, blank=True)
    image = models.ImageField(default='default.jpg', upload_to='profile_pics')

    def __str__(self):
        return f'{self.user.username} Profile'

    def save(self, *args, **kwargs):
        super(Profile, self).save(*args, **kwargs)

        img = Image.open(self.image.path)

        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size)
            img.save(self.image.path)


class Address(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    location = models.TextField(max_length=150, blank=True, null=True)
    lat = models.FloatField(blank=True, null=True)
    long = models.FloatField(blank=True, null=True)

    def __str__(self):
        return f'{self.user.username} Address'

    def save(self, *args, **kwargs):
        g = geocoder.mapbox(self.location, key=Config.MAPBOX_ACCESS_TOKEN)
        g = g.latlng # [lat, lng]
        self.lat = g[0]
        self.long = g[1]
        return super(Address, self).save(*args, **kwargs)
