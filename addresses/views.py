from django.shortcuts import render
from django.views.generic.edit import CreateView
from users.models import Address
from config import Config
# Create your views here.

class  AddressView(CreateView):
	model = Address
	fields = ['lat','long']
	template_name = 'addresses/home.html'
	success_url = '/'

	def get_context_data(self, **kwargs) :
		context = super().get_context_data(**kwargs)
		context['mapbox_access_token'] = Config.MAPBOX_ACCESS_TOKEN
		context['addresses'] = Address.objects.all()
		return context