"""portfolia configuration."""
from os import environ, path
from dotenv import load_dotenv

basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, '.env'))

class Config:
    """Base config."""
    # API_URL = environ.get('API_URL')
    DJANGO_HOST = environ.get('DJANGO_HOST')
    SECRET_KEY = environ.get('SECRET_KEY')
    LOG_FILE = environ.get('LOG_FILE')
    DEBUG = environ.get('DEBUG')
    LOG_LEVEL = environ.get('LOG_LEVEL')
    STATIC_ROOT = environ.get('STATIC_ROOT')
    MEADIA_ROOT = environ.get('MEADIA_ROOT')
    MYSQL_ROOT_PASSWORD = environ.get('MYSQL_ROOT_PASSWORD')
    MAPBOX_ACCESS_TOKEN = environ.get('MAPBOX_ACCESS_TOKEN')

class DNS:
    HOST = environ.get('MYSQL_HOST')
    USER = environ.get('MYSQL_USER')
    PASSWORD = environ.get('MYSQL_PASSWORD')
    DATABASE = environ.get('MYSQL_DATABASE')
    PORT = environ.get('MYSQL_PORT')
    MYSQL_CONN_TIMEOUT = environ.get('MYSQL_CONN_TIMEOUT')
    MYSQL_ROOT_PASSWORD = environ.get('MYSQL_ROOT_PASSWORD')
