FROM python:3.8
#MAINTAINER fiddler hlungwane "fhlungwane.cgn.co.za"
ENV APP_PATH /
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

## these are default and should be passed & overwtiten during build
ARG USER=user
ARG UID=1000
ARG GID=1000
# ARG PW=docker

## current build ARGs are echoed (so you can see them)
RUN echo "UID=${UID},GID=${GID},USER=${USER}"

## this will set permissions so we dont have to chown files & folders every time
RUN groupadd -f --gid ${GID} ${USER} && \
    useradd -m -s /bin/bash -u ${UID} -g ${GID} ${USER}

COPY requirements.txt requirements.txt

## create dirs for app
RUN mkdir -p /app
RUN chmod 777 /app


## install python pip requirements.txt
RUN pip install -U pip && \
    pip install -r requirements.txt

## set working dir to app ($APP_PATH)
WORKDIR /app

## copy project into /app
COPY --chown=${UID}:${GID} . .
## change to "user"
USER ${USER}
