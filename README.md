-- DJANGO PORTFOLIO:

- Django application that :
1) Extend the django user module to add further details for user profile, such as home address, phone number, location (point geometry) where they live.
2) An user profile page and a page to edit the user’s profile.
3) A page with full screen map that shows all registered users location.
4) When clicking the user icon (just use default icon), it will display the user’s profile popup
5) Users can log in from default Django admin page. Users can only see their own profile page, but not others.
6) User can only access Django admin page for all models by logging in as super user.

-- TECHNOLOGIES :
- Python3.8
- Django 3.x
- Gunicorn(for deployment)
- mysql
- Docker
- Linux/Ubuntu server

-- DEPLOYMENT :
- I have cattered for two ways to deploy this app:
1) With Linux & gunicorn(virtual env p)
2) Docker and gunicorn (Quickest approach)

- I will only focus on No.2
- There are Docker files in the root directory of the project, together with commands.txt(the commands to run, one after the other as they appear) , requirements.txt(required libaries) and .env(for configurations) files. However you need to have docker installed in your server. Please only make changes on the .env file. Replace DJANGO_HOST = 'your_ip_address'.
- After changing the DJANGO_HOST. Follow the instructions in the commands.txt.
- Please note we are using mysql server within docker, so after command $ -mysql -u root -p , a root password is empty, so just type Enter withiout typying anything. Then continue with those commands to set up superuser.

- After following all commands. then open your browser and type http://ypour_ip_address:8000/ and http://ypour_ip_address:8000/admin for you admin services.